package com.comact.hardware.protocol.SNMP;

import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.components.NetworkInterfaceCard;
import com.comact.hardware.testmocks.MOCreator;
import com.comact.hardware.testmocks.SnmpMockAgent;
import org.junit.Assert;
import org.snmp4j.PDU;
import org.snmp4j.smi.OID;

import java.io.IOException;

/**
 * Created by dparadis on 2016-05-31.
 */
public class SnmpTest {


    public void init() {
        System.out.println("Starting test");
    }


    public void Snmptest() {

        SnmpMockAgent mockAgent = null;
        SnmpManagerImpl manager;
        final OID sysDescr = new OID(".1.3.6.1.2.1.1.1.0");
        final OID getSysDescrNext = new OID(".1.3.6.1.2");


        try {
            mockAgent = new SnmpMockAgent("0.0.0.0");
            mockAgent.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Since BaseAgent registers some MIBs by default we need to unregister
        // one before we register our own sysDescr. Normally you would
        // override that method and register the MIBs that you need
        mockAgent.unregisterManagedObject(mockAgent.getSnmpv2MIB());

        // Register a system description, use one from you product environment
        // to test with
        mockAgent.registerManagedObject(MOCreator.createReadOnly(sysDescr,
                "This Description is set By ShivaSoft"));
        //agent.registerManagedObject(MOCreator.createReadOnly(getSysDescrNext,
        //        "See if it will return the Next");

        // Setup the client to use our newly started agent


        try {
            NetworkInterfaceCard nic = new NetworkInterfaceCard("127.0.0.1", NetworkCardType.Operation);
            manager = new SnmpManagerImpl(nic, "public");
            PDU response = manager.getSingle(sysDescr);
            System.out.println("Response: " + response.getVariableBindings().firstElement().getVariable());
            response = manager.getNext(getSysDescrNext);
            Assert.assertNotNull(response);
            System.out.println("With a get Next: " + response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //client.start();
        // Get back Value which is set
        //System.out.println(client.getAsString(sysDescr));
    }
}
