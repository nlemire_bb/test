package com.comact.hardware.components.networkSwitch;

import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.components.NetworkInterfaceCard;
import com.comact.hardware.protocol.SNMP.SnmpManager;
import com.comact.hardware.protocol.SNMP.SnmpManagerImpl;
import com.comact.hardware.testmocks.SnmpMockManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by dparadis on 2016-05-25.
 */
public class SwitchTest {

    private static final Logger LOGGER = Logger.getLogger(SwitchTest.class);
    private SnmpManager snmpManager;
    private NetworkInterfaceCard nic;

    @Before
    public void init() {

        String ipaddress = System.getenv("SWITCH_IP_ADDRESS");
        String community = System.getenv("SNMP_COMMUNITY");
        String saveToDisk = System.getenv("SAVE_PDU_ON_DISK");

        if (ipaddress != null && ipaddress.length() > 0) {
            nic = new NetworkInterfaceCard(ipaddress, NetworkCardType.Operation);
            if(community == null)
                community = "public";
            snmpManager = new SnmpManagerImpl(nic, community);
            if (saveToDisk != null && saveToDisk.equals("true")) {
                snmpManager.setSavePdutoDisk(true);
            }

        } else {
            snmpManager = new SnmpMockManager("Switch");
        }
    }

    @Test
    public void testSwitch() throws Exception {
        ArrayList<SwitchInterface> allports;

        Switch myswitch = new Switch(snmpManager);
        Assert.assertSame("Checking Switch Init", "OK", myswitch.init());
        allports = myswitch.getPorts();

        for (int port = 0; port < allports.size(); port++) {

            SwitchInterface currentPort = allports.get(port);
            if (currentPort.getOperationStatus() == 1) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("*******Port " + currentPort.getPortNum() + "*********");
                    LOGGER.debug("Name: " + currentPort.getName());
                    LOGGER.debug("State: UP");
                    LOGGER.debug("Speed: " + currentPort.getSpeed() / 1000000 + " mbit/sec");
                    LOGGER.debug("Vlan: " + currentPort.getVlan());

                    if (currentPort.getConnectedDevices().size() == 0) {
                        LOGGER.debug("MAC Address: No MAC Entry");
                    } else {
                        ArrayList<ConnectedDevice> devices = currentPort.getConnectedDevices();

                        for (int u = 0; u < devices.size(); u++) {
                            ConnectedDevice currentdevice = devices.get(u);
                            LOGGER.debug("MAC Address: " + currentdevice.getMacAddr());
                            if (currentdevice.getManufacturer() != null) {
                                LOGGER.debug("Manufacturer: " + currentdevice.getManufacturer());
                                LOGGER.debug("Type: " + currentdevice.getDeviceType());
                            }
                        }
                    }
                }
            }
        }
    }


}
