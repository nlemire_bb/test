package com.comact.hardware.components.ups;

import com.comact.hardware.protocol.SNMP.TrapListener;
import org.junit.Assert;
import org.junit.Test;
import org.snmp4j.smi.UdpAddress;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by dparadis on 2016-05-26.
 */
public class UpsTest {
    private final boolean TEST_HARDWARE = false;
    private final boolean LISTEN_TO_TRAPS = false;
    private final String UPS_IP = "10.121.199.16";
    private final String LISTENER_ADDRESS = "10.1.3.150";

    @Test
    public void testUPS() {

        Ups ups;
        if (TEST_HARDWARE) {
            ups = new UpsAPC(UPS_IP);
        }else{
            ups = mock(UpsAPC.class);
            when(ups.getStatus()).thenReturn("2");
        }

        Assert.assertEquals("Status Error","2",ups.getStatus());

        if (LISTEN_TO_TRAPS) {
            TrapListener snmp4jTrapReceiver = new TrapListener();
            try {
                snmp4jTrapReceiver.listen(new UdpAddress(LISTENER_ADDRESS +"/162"));

            } catch (IOException e) {
                System.err.println("Error in Listening for Trap");
                System.err.println("Exception Message = " + e.getMessage());
            }
        }
    }
}
