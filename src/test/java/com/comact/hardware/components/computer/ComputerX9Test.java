package com.comact.hardware.components.computer;

import com.comact.hardware.components.IpmiNetworkInterface;
import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.components.NetworkInterfaceCard;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * Created by dparadis on 2016-07-04.
 */
public class ComputerX9Test {

    private static final Logger LOGGER = Logger.getLogger(ComputerX9Test.class);
    private ArrayList<NetworkInterfaceCard> nics = new ArrayList<>();
    private NetworkInterfaceCard operNic;
    private NetworkInterfaceCard spiedOperNic;
    private IpmiNetworkInterface ipmiNic;
    private ComputerX9 computer;
    private boolean isMock;
    private String operIpAddress;
    private String ipmiIpAddress;
    private IpmiNetworkInterface spiedIPMINic;

    @Before
    public void setup()throws Exception {

        operIpAddress = System.getenv("OPERATION_IP_ADDRESS");
        ipmiIpAddress = System.getenv("IPMI_IP_ADDRESS");

        if (operIpAddress == null || operIpAddress.length() < 1 || ipmiIpAddress == null || ipmiIpAddress.length() < 1) {

            isMock = true;
            operIpAddress = "192.168.1.1";
            operNic = new NetworkInterfaceCard(operIpAddress, NetworkCardType.Operation);
            spiedOperNic = spy(operNic);
            nics.add(spiedOperNic);

            ipmiIpAddress = "192.168.1.1";
            ipmiNic = new IpmiNetworkInterface(ipmiIpAddress, NetworkCardType.Ipmi, 6000, "ADMIN", "ADMIN");
            spiedIPMINic = spy(ipmiNic);
            nics.add(spiedIPMINic);

        } else {
            isMock = false;
            operNic = new NetworkInterfaceCard(operIpAddress, NetworkCardType.Operation);
            nics.add(operNic);

            ipmiNic = new IpmiNetworkInterface(ipmiIpAddress, NetworkCardType.Ipmi, 6000, "ADMIN", "ADMIN");
            nics.add(ipmiNic);
        }
        computer = new ComputerX9(nics);
        computer.setType(ComputerType.Supermicro_X9_Node);
        computer.setComputerName("test");
    }

    @Test
    public void getStatusReturnsOK() {

        if (isMock == true) {
            doReturn(true).when(spiedOperNic).isReacheable();
        }
        Assert.assertEquals("OK", computer.getStatus());
    }

    @Test
    public void getStatusReturnsNotReachable() {

        if (isMock == true) {
            doReturn(false).when(spiedOperNic).isReacheable();
            Assert.assertEquals("Device Not Reachable", computer.getStatus());
        }
    }

    @Test
    public void restartCantConnect() throws Exception {

        if (isMock == true) {
            doReturn(false).when(spiedOperNic).isReacheable();
            doReturn(false).when(spiedIPMINic).isReacheable();

            try {
                computer.restart();
            } catch (Exception e) {
                Assert.assertEquals("java.lang.Exception: Target is already Offline & unreachable", e.toString());
            }

        }
    }

}
