package com.comact.hardware.components;

import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatus;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatusResponseData;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;
import com.veraxsystems.vxipmi.connection.ConnectionException;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Time;
import java.util.Date;

import static org.mockito.Mockito.*;

/**
 * Created by dparadis on 2016-07-06.
 */
public class IpmiNetworkInterfaceTest {
    private static final Logger LOGGER = Logger.getLogger(IpmiNetworkInterfaceTest.class);
    private IpmiConnector connection;
    private GetChassisStatusResponseData chassisData;
    private IpmiNetworkInterface ipmi;
    private ConnectionHandle handle;
    private CipherSuite cs;
    private GetChassisStatus chassisStatus;

    @Before
    public void setup() throws Exception {
        connection = mock(IpmiConnector.class);
        chassisData = mock(GetChassisStatusResponseData.class);
        handle = mock(ConnectionHandle.class);
        cs = mock(CipherSuite.class);
        chassisStatus = mock(GetChassisStatus.class);
        ipmi = spy(new IpmiNetworkInterface("127.0.0.1", NetworkCardType.Ipmi, 6001, "ADMIN", "ADMIN"));
        ipmi.setConnector(connection);
        ipmi.setCs(cs);
        ipmi.setHandle(handle);
        ipmi.setChassisStatus(chassisStatus);
        when((GetChassisStatusResponseData) connection.sendMessage(handle, chassisStatus)).thenReturn(chassisData);
    }

    @Test(expected = Exception.class)
    public void IMPMINoConnection() throws Exception {
        when(ipmi.isReacheable()).thenReturn(false);
        try {
            ipmi.turnON(2000);
        } catch (Exception e) {
            LOGGER.info(e);
            Assert.assertEquals("Wrong Exception: ", "java.lang.Exception: Could Not Connect to IPMI on:127.0.0.1 java.lang.Exception: Can't open Socket to 127.0.0.1", e.toString());
            throw new Exception(e);
        }
    }

    @Test
    public void IMPMIPowerCommandTurnONReturnTrue() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(false).thenReturn(false).thenReturn(false).thenReturn(true);
        try {
            ipmi.turnON(100);
        } catch (Exception e) {
            LOGGER.info(e);
            throw new Exception(e);
        }
    }

    @Test(expected = Exception.class)
    public void IMPMIPowerCommandTurnOFFWhileAlreadyOFF() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(false);
        try {
            ipmi.turnOFF(500);
        } catch (Exception e) {
            LOGGER.info(e);
            Assert.assertEquals("", "java.lang.Exception: Computer Already OFF", e.toString());
            throw new Exception(e);
        }

    }

    @Test
    public void IMPMIPowerCommandTurnOFFReturnTrue() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        try {
            ipmi.turnOFF(500);
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    @Test(expected = Exception.class)
    public void IMPMIPowerCommandTurnONWhileAlreadyON() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(true);
        try {
            ipmi.turnON(500);
        } catch (Exception e) {
            LOGGER.info(e);
            Assert.assertEquals("", "java.lang.Exception: Computer Already ON", e.toString());
            throw new Exception(e);
        }
    }

    @Test(expected = Exception.class)
    public void IMPMIPowerCommandTurnOFFAlwaysON() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(true);
        try {
            ipmi.turnOFF(500);
        } catch (Exception e) {
            LOGGER.info(e);
            Assert.assertEquals("", "java.lang.Exception: Computer never changed state", e.toString());
            throw new Exception(e);
        }
    }

    @Test(expected = Exception.class)
    public void IMPMIPowerCommandTurnONAlwaysOFF() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(false);
        try {
            ipmi.turnON(500);
        } catch (Exception e) {
            LOGGER.info(e);
            Assert.assertEquals("", "java.lang.Exception: Computer never changed state", e.toString());
            throw new Exception(e);
        }

    }

    @Test
    public void IMPMIGetStateReturnsON() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(true);
        Assert.assertSame(ComputerState.ON, ipmi.getState());
    }

    @Test
    public void IMPMIGetStateReturnsOFF() {

        when(ipmi.isReacheable()).thenReturn(true);
        when(chassisData.isPowerOn()).thenReturn(false);
        Assert.assertSame(ComputerState.OFF, ipmi.getState());
    }

    @Test
    public void IMPMIGetStateReturnsUnknown() throws Exception {

        when(ipmi.isReacheable()).thenReturn(true);
        when(connection.sendMessage(handle, chassisStatus)).thenThrow(new Exception("AHHHHHHHHH"));
        Assert.assertSame(ComputerState.UNKNOWN, ipmi.getState());
    }

    /*@Test
    public void test() throws Exception {
        ipmi = new IpmiNetworkInterface("10.121.199.23", NetworkCardType.Ipmi, 6010, "ADMIN", "ADMIN");
        int tries = 0;
        while (true) {
            long time = System.currentTimeMillis();

            ComputerState state = null;
            try {
                state = ipmi.getState();
            } catch (Exception e) {
                System.out.println(e);
            }
            System.out.print("Computer is: " + state.toString());

            long completedIn = System.currentTimeMillis() - time;
            System.out.println(" completed in : " + completedIn+" "+tries++);

            Thread.sleep(500);
        }

    }*/

}
