package com.comact.hardware.components;

import com.comact.hardware.components.computer.Computer;
import com.comact.hardware.components.computer.ComputerX9;
import com.comact.hardware.components.webrelay.WebRelay;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by dparadis on 2016-06-08.
 */
public class RebootScript {
    private static final Logger LOGGER = Logger.getLogger(RebootScript.class);

    private ArrayList<Computer> computers = new ArrayList<>();

    //@Test
    public void systemRestart() {
        ArrayList<Future> futures = new ArrayList<>();
        ExecutorService threads = Executors.newFixedThreadPool(10);
        NetworkInterfaceCard nic = new NetworkInterfaceCard("10.121.108.106", NetworkCardType.Operation);
        WebRelay lmiRelay = new WebRelay(nic);
        createComputer("10.121.108.2", "Controller", "10.121.108.153");
        createComputer("10.121.108.3", "Vision 1", "10.121.108.154");
        createComputer("10.121.108.4", "Vision 2", "10.121.108.155");
        createComputer("10.121.108.5", "Vision 3", "10.121.108.156");
        createComputer("10.121.108.6", "Vision 4", null);
        createComputer("10.121.108.7", "Vision 5", null);
        createComputer("10.121.108.101", "LMI Station", "10.121.108.151");

        try {
            lmiRelay.restart();
        } catch (
                Exception e
                )

        {
            LOGGER.info(e);
        }

        for (Computer computer : computers) {

            Future future = threads.submit(new Runnable() {
                @Override
                public void run(){
                    try {
                        computer.restart();
                    } catch (Exception e) {
                        LOGGER.error("Can't start Thread");
                    }
                }
            });
            futures.add(future);
        }

        boolean threadsDone = false;

        while (!threadsDone){

            int i = 0;
            for (Future future : futures) {
                if (future.isDone()) i++;
                if (i >= futures.size()) {
                    threadsDone = true;
                }
            }
            for (Computer computer : computers) {
                System.out.println(computer.getComputerName() + " status = " + computer.getRestartStatus());
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOGGER.error("Thread Can't sleep", e);
            }
        }

        threads.shutdown();
    }

    private void createComputer(String operAddr, String name, String ipmiAddr) {

        ArrayList<NetworkInterfaceCard> nics = new ArrayList<>();

        if (ipmiAddr != null) {
            IpmiNetworkInterface ipminic = null;
            try {
                ipminic = new IpmiNetworkInterface(ipmiAddr, NetworkCardType.Ipmi, 6000, "ADMIN", "ADMIN");
            } catch (Exception e) {
                LOGGER.error("Could not initialize IPMI: Check Configuration",e);
            }
            nics.add(ipminic);
        }

        NetworkInterfaceCard nic = new NetworkInterfaceCard(operAddr, NetworkCardType.Operation);
        nics.add(nic);
        ComputerX9 computer = new ComputerX9(nics);
        computer.setComputerName(name);
        computers.add(computer);
    }
}

