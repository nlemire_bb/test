package com.comact.hardware.components.webrelay;

import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.components.NetworkInterfaceCard;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.*;

/**
 * Created by dparadis on 2016-05-20.
 */
public class WebRelayTest {

    private static final Logger LOGGER = Logger.getLogger(WebRelayTest.class);
    private String ipaddress;
    private WebRelay webRelay;
    private NetworkInterfaceCard nic;
    private WebRequestImpl webRequest;
    private boolean isMock;
    private final static String TURN_RELAY_ON_CMD = "?relayState=0 HTTP/1.1\\nAuthorization: Basic bm9uZTp3ZWJyZWxheQ==\\r\\n\\r\\n";
    private final static String TURN_RELAY_OFF_CMD = "?relayState=1 HTTP/1.1\\nAuthorization: Basic bm9uZTp3ZWJyZWxheQ==\\r\\n\\r\\n";
    private String RELAY_STATE_URL;

    @Before
    public void setup() throws IOException {

        ipaddress = System.getenv("WEB_RELAY_IP_ADDRESS");
        if (ipaddress == null || ipaddress.length() < 1) {
            ipaddress = "192.168.1.1";
            nic = mock(NetworkInterfaceCard.class);
            when(nic.getIp()).thenReturn(ipaddress);
            webRelay = new WebRelay(nic);
            webRequest = mock(WebRequestImpl.class);
            webRelay.setWebRelayStatusReqTest(webRequest);
            isMock = true;
        } else {
            isMock = false;
            nic = new NetworkInterfaceCard(ipaddress, NetworkCardType.Operation);
            nic.setReacheable(true);
            webRelay = new WebRelay(nic);
        }
        RELAY_STATE_URL = "http://" + ipaddress + "/state.xml";
    }

    @Test
    public void getStatusReturnsOK() throws Exception {
        if (isMock) {
            Object bytes = loadMockFile("statebytesON");
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
            when(nic.isReacheable()).thenReturn(true);
        }
        WebRelayStatus result = null;
        try {
            result = webRelay.getStatus();
        } catch (Exception e) {
            LOGGER.error(e);
        }
        Assert.assertSame("Bad Status", WebRelayStatus.OK, result);
    }

    @Test(expected = Exception.class)
    public void getStateReturnNull() throws Exception {
        String result;
        if (isMock) {
            Object bytes = loadMockFile("statecorrupted");
            when(nic.isReacheable()).thenReturn(true);

            //returns corrupted data
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
            result = webRelay.getState();
            Assert.assertSame("should have returned a null response from webrelay", null, result);

            //returns null
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn(null);
            try {
                result = webRelay.getState();
            } catch (Exception e) {
                throw new Exception(e);
            }
            Assert.assertSame("should have returned a null response from webrelay", null, result);
        } else
            throw new Exception("Exception Bidon Lorsqu'on test le hardware");
    }

    @Test
    public void getStateUnreachable() throws Exception {

        if (isMock) {
            WebRelayStatus result = webRelay.getStatus();
            Assert.assertEquals("should have returned a null response from webrelay", WebRelayStatus.UNREACHEABLE, result);
        }
    }

    @Test
    public void turnRelayONReturnOK() throws Exception {
        if (isMock) {
            Object bytes = loadMockFile("statebytesON");
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
        }
        webRelay.turnON();

        if (isMock)
            verify(webRequest).sendCmd(RELAY_STATE_URL + TURN_RELAY_ON_CMD);
    }


    @Test
    public void turnRelayOFFReturnOK() throws Exception {
        if (isMock) {
            Object bytes = loadMockFile("statebytesOFF");
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
        }
        webRelay.turnOFF();
        if (isMock)
            verify(webRequest).sendCmd(RELAY_STATE_URL + TURN_RELAY_OFF_CMD);
    }

    @Test(expected = Exception.class)
    public void turnRelayOnReturnsException() throws Exception {
        if (isMock) {
            //turn relay ON but return OFF
            Object bytes = null;
            try {
                bytes = loadMockFile("statebytesOFF");
                when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
            } catch (Exception e) {
                LOGGER.error("Mockito Test Error");
            }
            try {
                webRelay.turnON();
            } catch (Exception e) {
                LOGGER.info(e);
                Assert.assertEquals("Wrong exception: ", "java.lang.Exception: State did not change", e.toString());
                throw new Exception(e);
            }
        } else
            throw new Exception("Exception Bidon Lorsqu'on test le hardware");
    }

    @Test(expected = Exception.class)
    public void turnRelayOffReturnsException() throws Exception {
        if (isMock) {
            Object bytes;
            //turn Relay OFF but return ON
            try {
                bytes = loadMockFile("statebytesON");
                when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
            } catch (Exception e) {
                LOGGER.error("Mockito Error");
            }
            try {
                webRelay.turnOFF();
            } catch (Exception e) {
                LOGGER.info(e);
                Assert.assertEquals("Wrong exception: ", "java.lang.Exception: State did not change", e.toString());
                throw new Exception(e);
            }
        } else
            throw new Exception("Exception Bidon Lorsqu<on test le hardware");
    }

    @Test
    public void powerCycleReturnsTrue() throws Exception {
        if (isMock) {
            try {
                Object bytesOFF = loadMockFile("statebytesOFF");
                Object bytesON = loadMockFile("statebytesON");
                when(webRequest.getXml(RELAY_STATE_URL))
                        .thenReturn((ByteArrayOutputStream) bytesOFF)
                        .thenReturn((ByteArrayOutputStream) bytesON);
            } catch (Exception e) {
                LOGGER.error("Mockito Test Error");
            }
        }
        try {
            webRelay.restart();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @Test(expected = Exception.class)
    public void powerCycleWontTurnOFF() throws Exception {
        if (isMock) {
            try {
                Object bytesON = loadMockFile("statebytesON");
                when(webRequest.getXml(RELAY_STATE_URL))
                        .thenReturn((ByteArrayOutputStream) bytesON)
                        .thenReturn((ByteArrayOutputStream) bytesON);
            } catch (IOException e) {
                LOGGER.error("Mockito test error");
            }
            try {
                webRelay.restart();
            } catch (Exception e) {
                LOGGER.info(e);
                Assert.assertEquals("Wrong exception: ","java.lang.Exception: State did not change",e.toString());
                throw new Exception(e);

            }
        } else
            throw new Exception("Exception Bidon Lorsqu'on test le hardware");
    }

    @Test(expected = Exception.class)
    public void powerCycleWontTurnON() throws Exception {
        if (isMock) {
            try {
                Object bytesOFF = loadMockFile("statebytesOFF");
                when(webRequest.getXml(RELAY_STATE_URL))
                        .thenReturn((ByteArrayOutputStream) bytesOFF)
                        .thenReturn((ByteArrayOutputStream) bytesOFF);
            } catch (Exception e) {
                LOGGER.error("Mockito test error");
            }
            try {
                webRelay.restart();
            } catch (Exception e) {
                LOGGER.info(e);
                Assert.assertEquals("Wrong exception: ", "java.lang.Exception: State did not change",e.toString());
                throw new Exception(e);
            }
        } else
            throw new Exception("Exception Bidon Lorsqu'on test le hardware");
    }

    @Test
    public void getRelayStateReturnON() throws Exception {
        String result;
        if (isMock) {
            try {
                Object bytes = loadMockFile("statebytesON");
                when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
            } catch (Exception e) {
                LOGGER.error("Mockito Test Error");
            }
        }

        webRelay.turnON();
        result = webRelay.getState();
        Assert.assertSame("Relay should be ON", "ON", result);
    }

    @Test
    public void getRelayStateReturnOFF() throws Exception {
        String result;
        if (isMock) {
            Object bytes = loadMockFile("statebytesOFF");
            when(webRequest.getXml(RELAY_STATE_URL)).thenReturn((ByteArrayOutputStream) bytes);
        }
        webRelay.turnOFF();
        result = webRelay.getState();
        Assert.assertSame("Relay should be OFF", "OFF", result);
    }

    private Object loadMockFile(String filename) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(filename);
        XStream xstream = new XStream();
        Object bytes = xstream.fromXML(inputStream);
        inputStream.close();
        return bytes;
    }
}
