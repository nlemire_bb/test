package com.comact.hardware.testmocks;

import com.comact.hardware.protocol.SNMP.SnmpManager;
import com.thoughtworks.xstream.XStream;
import org.snmp4j.PDU;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.InputStream;

/**
 * Created by dparadis on 2016-05-25.
 */
public class SnmpMockManager implements SnmpManager {
    private int id = 0;
    private String mocktype;

    public SnmpMockManager(String type) {
        mocktype = type;
    }

    @Override
    public PDU getSingle(OID oid) throws Exception {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(new OID(oid)));
        PDU response = snmpGet(pdu, "get");
        return response;
    }

    @Override
    public PDU getNext(OID oid) throws Exception {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(new OID(oid)));
        PDU response = snmpGet(pdu, "getNext");
        return response;
    }

    @Override
    public PDU getBulk(PDU pdu, String type) throws Exception {
        PDU response;
        if (type.equals("Next")) {
            response = snmpGet(pdu, "getBulkNext");
        } else {
            response = snmpGet(pdu, "getBulk");
        }
        return response;
    }

    @Override
    public PDU snmpGet(PDU pdu, String type) throws Exception {
        PDU loadedpdu= null;
        pdu.setRequestID(new Integer32(++id));
        String filename = "pdu" + pdu.getRequestID() + ".txt";
        InputStream inputStream = getClass().getResourceAsStream(filename);
        XStream xstream = new XStream();
        loadedpdu = (PDU) xstream.fromXML(inputStream);
        inputStream.close();
        return loadedpdu;
    }

    @Override
    public void savePDUtoDisk(PDU pdu, String filename) {
        throw new NotImplementedException();
    }

    @Override
    public void setSavePdutoDisk(boolean savePdutoDisk) {
        throw new NotImplementedException();
    }

}
