package com.comact.hardware.protocol.telnet;

import org.apache.commons.net.telnet.TelnetClient;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dparadis on 2016-05-19.
 */
public class TelnetListener implements Runnable {

    boolean loop;

    TelnetClient tnclient;

    public TelnetListener(TelnetClient tn) {
        tnclient = tn;
        loop = true;
    }

    public void stopThread() {
        loop = false;
    }

    @Override
    public void run() {

        InputStream instr = tnclient.getInputStream();
        while (loop == true) {
            try {
                byte[] buff = new byte[1024];
                int returnRead = 0;

                do {
                    returnRead = instr.read(buff);
                    if (returnRead > 0) {
                        System.out.print(new String(buff, 0, returnRead));
                    }
                }
                while (returnRead >= 0);

            } catch (IOException e) {
                System.err.println("Exception while reading socket:" + e.getMessage());
                loop = false;
            }
        }
        System.out.println("Reader Thread has stopped");
        try {
            tnclient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
