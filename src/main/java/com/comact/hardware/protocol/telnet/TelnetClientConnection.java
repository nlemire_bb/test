package com.comact.hardware.protocol.telnet;

/*
 *
 */

import com.comact.hardware.components.NetworkInterfaceCard;
import org.apache.commons.net.telnet.*;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;


/***
 * This is a simple example of use of TelnetClientConnection.
 * An external option handler (SimpleTelnetOptionHandler) is used.
 * Initial configuration requested by TelnetClientConnection will be:
 * WILL ECHO, WILL SUPPRESS-GA, DO SUPPRESS-GA.
 * VT100 terminal type will be subnegotiated.
 * <p>
 * Also, use of the sendAYT(), getLocalOptionState(), getRemoteOptionState()
 * is demonstrated.
 * When connected, type AYT to send an AYT command to the server and see
 * the result.
 * Type OPT to see a report of the state of the first 25 options.
 ***/
public class TelnetClientConnection implements TelnetNotificationHandler {
    private static final Logger LOGGER = Logger.getLogger(TelnetClientConnection.class);
    org.apache.commons.net.telnet.TelnetClient tc = null;
    OutputStream outstr;
    final static char CR = (char) 0x0D;//enter Char
    final static char LF = (char) 0x0A;//Line feed char
    boolean endLoop;
    FileOutputStream fout = null;
    TelnetListener reader;
    Thread thread;
    String tftpIP = "10.125.2.158";
    private String ip;

    public TelnetClientConnection(NetworkInterfaceCard nic) {
        this.ip = nic.getIp();
        tc = new org.apache.commons.net.telnet.TelnetClient();
        TerminalTypeOptionHandler ttopt = new TerminalTypeOptionHandler("VT100", false, false, true, false);
        EchoOptionHandler echoopt = new EchoOptionHandler(true, false, true, false);
        SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);
        try {
            tc.addOptionHandler(ttopt);
            tc.addOptionHandler(echoopt);
            tc.addOptionHandler(gaopt);
        } catch (Exception e) {
            System.err.println("Error registering option handlers: " + e.getMessage());
        }
    }

    public boolean connect() {
        try {
            tc.connect(ip, 23);
            outstr = tc.getOutputStream();
            return true;
        } catch (IOException e) {
            LOGGER.error("Can't connect", e);
            return false;
        }
    }

    /*public void registerNotifHandler() {
        try {
            tc.registerNotifHandler(new TelnetClientConnection());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void sendtohost(byte[] bytesToSend) {
        try {
            outstr.write(bytesToSend, 0, bytesToSend.length);
            outstr.flush();
        } catch (IOException e) {
            System.out.println("could not login");
        }
    }

    public void login(String passwd, String usrname) {
        //byte[] byteBuffer = new String(usrname + CR + passwd + CR + "en" + CR + passwd + CR).getBytes();
        //byte[] byteBuffer = new String(CR + usrname + CR + passwd + CR + "en").getBytes();
        byte[] byteBuffer = (usrname + CR + passwd + CR).getBytes();
        sendtohost(byteBuffer);
    }

    public void sendCmdtoHost(String cmd) {
        byte[] byteBuffer = (cmd + CR).getBytes();
        sendtohost(byteBuffer);
    }

    public void logout() {
        byte[] byteBuffer = ("exit" + CR).getBytes();
        sendtohost(byteBuffer);
    }

    public void startReader() {
        reader = null;
        try {
            reader = new TelnetListener(tc);
            thread = new Thread(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread.setName("TelnetListener");
        thread.setPriority(10);
        thread.start();
    }

    public void stopReader() {
        if (reader != null) {
            reader.stopThread();
        }
    }

    public void emulateTerm() {

        try {
            byte[] buff = new byte[1024];
            int returnRead = 0;

            startReader();

            do {
                try {
                    returnRead = System.in.read(buff);
                    if (returnRead > 0) {
                        final String line = new String(buff, 0, returnRead); // deliberate use of default charset
                        if (line.startsWith("AYT")) {
                            try {
                                System.out.println("Sending AYT");

                                System.out.println("AYT response:" + tc.sendAYT(5000));
                            } catch (IOException e) {
                                System.err.println("Exception waiting AYT response: " + e.getMessage());
                            }
                        } else if (line.startsWith("OPT")) {
                            System.out.println("Status of options:");
                            for (int ii = 0; ii < 25; ii++) {
                                System.out.println("Local Option " + ii + ":" + tc.getLocalOptionState(ii) +
                                        " Remote Option " + ii + ":" + tc.getRemoteOptionState(ii));
                            }
                        } else if (line.startsWith("REGISTER")) {
                            StringTokenizer st = new StringTokenizer(new String(buff));
                            try {
                                st.nextToken();
                                int opcode = Integer.parseInt(st.nextToken());
                                boolean initlocal = Boolean.parseBoolean(st.nextToken());
                                boolean initremote = Boolean.parseBoolean(st.nextToken());
                                boolean acceptlocal = Boolean.parseBoolean(st.nextToken());
                                boolean acceptremote = Boolean.parseBoolean(st.nextToken());
                                SimpleOptionHandler opthand = new SimpleOptionHandler(opcode, initlocal, initremote,
                                        acceptlocal, acceptremote);
                                tc.addOptionHandler(opthand);
                            } catch (Exception e) {
                                if (e instanceof InvalidTelnetOptionException) {
                                    System.err.println("Error registering option: " + e.getMessage());
                                } else {
                                    System.err.println("Invalid REGISTER command.");
                                    System.err.println("Use REGISTER optcode initlocal initremote acceptlocal acceptremote");
                                    System.err.println("(optcode is an integer.)");
                                    System.err.println("(initlocal, initremote, acceptlocal, acceptremote are boolean)");
                                }
                            }
                        } else if (line.startsWith("UNREGISTER")) {
                            StringTokenizer st = new StringTokenizer(new String(buff));
                            try {
                                st.nextToken();
                                int opcode = (new Integer(st.nextToken())).intValue();
                                tc.deleteOptionHandler(opcode);
                            } catch (Exception e) {
                                if (e instanceof InvalidTelnetOptionException) {
                                    System.err.println("Error unregistering option: " + e.getMessage());
                                } else {
                                    System.err.println("Invalid UNREGISTER command.");
                                    System.err.println("Use UNREGISTER optcode");
                                    System.err.println("(optcode is an integer)");
                                }
                            }
                        } else if (line.startsWith("LOGIN")) {
                            login("mm98", "comact");
                        } else if (line.startsWith("BACKUP")) {
                            backupConfig();
                        } else if (line.startsWith("QUIT")) {
                            reader.stopThread();
                        } else if (line.matches("^\\^[A-Z^]\\r?\\n?$")) {
                            byte toSend = buff[1];
                            if (toSend == '^') {
                                outstr.write(toSend);
                            } else {
                                outstr.write(toSend - 'A' + 1);
                            }
                            outstr.flush();
                        } else {
                            try {
                                outstr.write(buff, 0, returnRead);
                                outstr.flush();
                            } catch (IOException e) {
                                endLoop = true;
                            }
                        }
                    }
                } catch (IOException e) {
                    System.err.println("Exception while reading keyboard:" + e.getMessage());
                    endLoop = true;
                }
            }
            while ((returnRead > 0) && (endLoop == false));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void backupConfig() {
        DateFormat dateFormat = new SimpleDateFormat("ddMMMyy_HH_mm");
        Date date = new Date();

        byte[] cliBackup = ("copy running-config tftp:" + CR
                + tftpIP + CR
                + "BackupConfig" + dateFormat.format(date) + CR).getBytes();
        sendtohost(cliBackup);
        System.out.println("Backup Command has been sent");
    }

    public void restoreConfig(String filename) {

        byte[] cliBackup = ("copy tftp: running-config" + CR
                + tftpIP + CR
                + filename + CR + "running-config" + CR).getBytes();
        sendtohost(cliBackup);
    }


    /***
     * Callback method called when TelnetClientConnection receives an option
     * negotiation command.
     *
     * @param negotiationCode - type of negotiation command received
     *                        (RECEIVED_DO, RECEIVED_DONT, RECEIVED_WILL, RECEIVED_WONT, RECEIVED_COMMAND)
     * @param option_code     - code of the option negotiated
     ***/
    @Override
    public void receivedNegotiation(int negotiationCode, int option_code) {
        String command = null;
        switch (negotiationCode) {
            case TelnetNotificationHandler.RECEIVED_DO:
                command = "DO";
                break;
            case TelnetNotificationHandler.RECEIVED_DONT:
                command = "DONT";
                break;
            case TelnetNotificationHandler.RECEIVED_WILL:
                command = "WILL";
                break;
            case TelnetNotificationHandler.RECEIVED_WONT:
                command = "WONT";
                break;
            case TelnetNotificationHandler.RECEIVED_COMMAND:
                command = "COMMAND";
                break;
            default:
                command = Integer.toString(negotiationCode); // Should not happen
                break;
        }
        //System.out.println("Received " + command + " for option code " + option_code);
    }


}
