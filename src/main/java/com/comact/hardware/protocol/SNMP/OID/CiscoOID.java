package com.comact.hardware.protocol.SNMP.OID;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-05-11.
 */
public enum CiscoOID {
    IF_NUMBER(new VariableBinding(new OID().append("1.3.6.1.2.1.2.1.0"))),//number of IF with a single get
    IF_SPEED(new VariableBinding(new OID().append("1.3.6.1.2.1.2.2.1.5"))),// add Index to get Interface SPEED
    IF_DESCRIPTION(new VariableBinding(new OID().append("1.3.6.1.2.1.2.2.1.2"))),// add Index to get Interface description
    IF_OPRER_STATUS(new VariableBinding(new OID().append("1.3.6.1.2.1.2.2.1.8"))),// add Index to get Interface Status
    MACADDR(new VariableBinding(new OID().append("1.3.6.1.2.1.17.4.3.1.1"))), //
    MACPORT(new VariableBinding(new OID().append("1.3.6.1.2.1.17.4.3.1.2"))),
    IF_INDEX(new VariableBinding(new OID().append("1.3.6.1.2.1.2.2.1.1"))),
    IF_TYPE(new VariableBinding(new OID().append("1.3.6.1.2.1.2.2.1.3"))),
    IF_VLAN(new VariableBinding(new OID().append("1.3.6.1.4.1.9.9.68.1.2.2.1.2"))), // add index to get VLAN for a perticular port
    CRC_ERRORS(new VariableBinding(new OID().append("1.3.6.1.4.1.9.2.2.1.1.12")));

    private final VariableBinding oid;

    CiscoOID(VariableBinding oid) {
        this.oid = oid;
    }

    public OID getOid() {
        OID ciscoOid = oid.getOid();
        return ciscoOid;
    }

    public VariableBinding getVariableBinding() {
        return oid;
    }

    public String getStringOid() {
        String ciscoOid = oid.toString();
        return ciscoOid;
    }
}
