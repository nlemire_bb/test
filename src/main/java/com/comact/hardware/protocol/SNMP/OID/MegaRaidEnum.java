package com.comact.hardware.protocol.SNMP.OID;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-06-02.
 */
public enum MegaRaidEnum {
    CONTROLLER_NAME(new VariableBinding(new OID().append("1.3.6.1.4.1.3582.4.1.1.0"))),
    OS_VERSION(new VariableBinding(new OID().append("1.3.6.1.4.1.3582.4.1.2.0"))),
    COMPUTER_TYPE(new VariableBinding(new OID().append("1.3.6.1.4.1.3582.4.1.4.1.3.1.12.0")));
    private final VariableBinding oid;

    MegaRaidEnum(VariableBinding oid) {
        this.oid = oid;
    }

    public OID getOid() {
        return oid.getOid();
    }

    public VariableBinding getVariableBinding() {
        return oid;
    }

    public String getStringOid() {
        return oid.toString();
    }
}