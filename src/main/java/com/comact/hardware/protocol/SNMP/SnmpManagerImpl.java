package com.comact.hardware.protocol.SNMP;

import com.comact.hardware.components.NetworkInterfaceCard;
import com.thoughtworks.xstream.XStream;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by dparadis on 2016-04-21.
 */
public class SnmpManagerImpl implements SnmpManager {

    private static int snmpVersion = SnmpConstants.version2c;
    private PDU returnPDUresponse;
    private TransportMapping transport;
    private Snmp snmp;
    private CommunityTarget comtarget;
    private boolean saveToDiskPermission;
    private int id;

    public SnmpManagerImpl(NetworkInterfaceCard nic, String community) {

        comtarget = new CommunityTarget();
        comtarget.setCommunity(new OctetString(community));
        comtarget.setVersion(snmpVersion);
        comtarget.setAddress(new UdpAddress(nic.getIp()+"/"+161));
        comtarget.setRetries(2);
        comtarget.setTimeout(1000);

    }

    public void setSavePdutoDisk(boolean savePdutoDisk) {
        this.saveToDiskPermission = savePdutoDisk;
    }

    @Override
    public PDU getSingle(OID oid) throws Exception {

        PDU pdu = new PDU();
        pdu.add(new VariableBinding(new OID(oid)));
        PDU response = snmpGet(pdu, "get");
        return response;
    }

    @Override
    public PDU getNext(OID oid) throws Exception {

        PDU pdu = new PDU();
        pdu.add(new VariableBinding(new OID(oid)));
        PDU response = snmpGet(pdu, "getNext");
        return response;
    }

    @Override
    public PDU getBulk(PDU pdu, String type) throws Exception {
        PDU response;
        if (type.equals("Next")) {
            response = snmpGet(pdu, "getNext");
        } else {
            response = snmpGet(pdu, "get");
        }
        return response;
    }


    @Override
    public PDU snmpGet(PDU pdu, String type) throws Exception {

        transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
        transport.listen();

        // Create the PDU object

        pdu.setRequestID(new Integer32(++id));
        ResponseEvent response;

        if (type.equals("getNext")) {
            response = snmp.getNext(pdu, comtarget);
        } else {
            response = snmp.get(pdu, comtarget);
        }

        // Process Agent Response
        if (response != null) {
            PDU responsePDU = response.getResponse();
            if (saveToDiskPermission) {
                savePDUtoDisk(responsePDU, "pdu" + responsePDU.getRequestID() + ".txt");
            }

            if (responsePDU != null) {
                int errorStatus = responsePDU.getErrorStatus();
                int errorIndex = responsePDU.getErrorIndex();
                String errorStatusText = responsePDU.getErrorStatusText();

                if (errorStatus == PDU.noError) {
                    returnPDUresponse = responsePDU;
                } else {
                    System.out.println("Error: Request Failed");
                    System.out.println("Error Status = " + errorStatus);
                    System.out.println("Error Index = " + errorIndex);
                    System.out.println("Error Status Text = " + errorStatusText);
                }
            } else {
                System.out.println("Error: Response PDU is null");
            }
        } else {
            System.out.println("Error: Agent Timeout... ");
        }
        snmp.close();

        return returnPDUresponse;
    }

    @Override
    public void savePDUtoDisk(PDU pdu, String filename) {

        try {
            XStream xstream = new XStream();
            xstream.toXML(pdu, new FileOutputStream(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
