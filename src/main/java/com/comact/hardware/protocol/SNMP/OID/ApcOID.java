package com.comact.hardware.protocol.SNMP.OID;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-05-11.
 */
public enum ApcOID {
    GENERAL_STATUS(new VariableBinding(new OID().append("1.3.6.1.4.1.318.1.1.1.11.1.1.0"))),
    BATTERY_STATUS (new VariableBinding(new OID().append("1.3.6.1.4.1.318.1.1.1.2.1.1.0"))),
    TRAP_MSG (new VariableBinding(new OID().append("1.3.6.1.4.1.318.2.3")));

    private final VariableBinding oid;

    ApcOID(VariableBinding oid) {
        this.oid = oid;
    }

    public OID getOid() {
        OID oid = this.oid.getOid();
        return oid;
    }
    public VariableBinding getVariableBinding(){
        return oid;
    }
    public String getStringOid() {
        String oid = this.oid.toString();
        return oid;
    }
}

