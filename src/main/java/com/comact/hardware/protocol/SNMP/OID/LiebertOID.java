package com.comact.hardware.protocol.SNMP.OID;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-05-11.
 */
public enum LiebertOID {

        OUTPUT_SOURCE("1.3.6.1.2.1.33.1.4.1.0");

        private final String oid;

        LiebertOID(String oid) {
            this.oid = oid;
        }

        public OID getOid() {
            OID ciscoOid = new OID().append(oid);
            return ciscoOid;
        }
        public VariableBinding getVariableBinding(){
            VariableBinding varbin = new VariableBinding(new OID(oid));
            return varbin;
        }
        public String getStringOid() {
            return oid;
        }
    }
