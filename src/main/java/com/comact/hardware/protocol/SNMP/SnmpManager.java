package com.comact.hardware.protocol.SNMP;

import org.snmp4j.PDU;
import org.snmp4j.smi.OID;

/**
 * Created by dparadis on 2016-05-25.
 */
public interface SnmpManager {
    PDU getSingle(OID oid) throws Exception;

    PDU getNext(OID oid) throws Exception;

    PDU getBulk(PDU pdu, String type) throws Exception;

    PDU snmpGet(PDU pdu, String type) throws Exception;

    void savePDUtoDisk(PDU pdu, String filename);

    void setSavePdutoDisk(boolean savePdutoDisk);
}
