package com.comact.hardware.protocol.SNMP.OID;

import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-06-02.
 */
public enum MicrosoftEnum {
    PROCESS_LIST(new VariableBinding(new OID().append("1.3.6.1.2.1.25.4.2.1.2"))),
    STORAGE(new VariableBinding(new OID().append("1.3.6.1.2.1.25.2.3.1")));

    private final VariableBinding oid;

    MicrosoftEnum(VariableBinding oid) {
        this.oid = oid;
    }

    public OID getOid() {
        OID ciscoOid = oid.getOid();
        return ciscoOid;
    }

    public VariableBinding getVariableBinding() {
        return oid;
    }

    public String getStringOid() {
        String ciscoOid = oid.toString();
        return ciscoOid;
    }
}
