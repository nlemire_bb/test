package com.comact.hardware.components;

/**
 * Created by dparadis on 2016-07-05.
 */
public interface CmocComponent {

    String getStatus()throws Exception;

    void restart() throws Exception;

    void turnON() throws Exception;

    void turnOFF() throws Exception;

}
