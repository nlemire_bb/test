package com.comact.hardware.components.computer;

import com.comact.hardware.components.IpmiNetworkInterface;
import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.protocol.telnet.TelnetClientConnection;
import com.comact.hardware.components.NetworkInterfaceCard;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created by dparadis on 2016-06-30.
 */
public class ComputerX9 extends Computer {
    private static final Logger LOGGER = Logger.getLogger(ComputerX9.class);
    private TelnetClientConnection telnetConnection;
    private NetworkInterfaceCard operationNic;
    private IpmiNetworkInterface ipmiNic;

    public ComputerX9(ArrayList<NetworkInterfaceCard> nics) {
        super(nics);
        operationNic = super.nics.get(NetworkCardType.Operation);
        ipmiNic = (IpmiNetworkInterface) super.nics.get(NetworkCardType.Ipmi);
    }

    @Override
    public String getStatus() {

        if (operationNic.isReacheable()) {
            status = "OK";
        } else status = "Device Not Reachable";

        return status;
    }

    @Override
    public void restart() throws Exception {

    }

    @Override
    public void turnON() throws Exception {

        if (ipmiNic == null) {
            throw new Exception("Operation Not supported without IPMI");
        }

        if (ipmiNic.isReacheable()) {

            ipmiNic.turnON(2000);

        } else {
            throw new Exception("IPMI Address can't be reached");
        }
    }

    @Override
    public void turnOFF() throws Exception {
        if (ipmiNic == null) {
            throw new Exception("Operation Not supported without IPMI");
        }

        if (ipmiNic.isReacheable()) {

            ipmiNic.turnOFF(2000);

        } else {
            throw new Exception("IPMI Address can't be reached");
        }
    }
}
