package com.comact.hardware.components.computer;

import com.comact.hardware.components.CmocComponent;
import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.components.NetworkInterfaceCard;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dparadis on 2016-06-13.
 */
public abstract class Computer implements CmocComponent {

    private static final Logger LOGGER = Logger.getLogger(Computer.class);

    protected String computerName;
    private ComputerType type;
    protected String status;
    protected String restartStatus;
    protected Map<NetworkCardType, NetworkInterfaceCard> nics = new HashMap<>();

    public Computer(ArrayList<NetworkInterfaceCard> nics) {

        for (NetworkInterfaceCard nic : nics){
            this.nics.put(nic.getType(), nic);
        }
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public ComputerType getType() {
        return type;
    }

    public void setType(ComputerType type) {
        this.type = type;
    }

    public String getRestartStatus() {
        return restartStatus;
    }

}
