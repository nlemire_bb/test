package com.comact.hardware.components.computer;

/**
 * Created by dparadis on 2016-07-07.
 */
public enum ComputerType {

    BridgeIbm(0),
    Ibm3650(1),
    Ibm3250(2),
    SuperMicro_X8(3),
    Corvalent(4),
    Gloria(5),
    Maxime(6),
    Supermicro_X9_Node(7),
    Supermicro_X9_Storage(8),
    Supermicro_X9_1U(9),
    Nuc(10),
    Remote_Lenovo(11),
    Stealth(12),
    Supermicro_X10(13),
    Wyse(14);

    private static final int BRIDGEIBM = 0;
    private static final int IBM3650 = 1;
    private static final int IBM3250 = 2;
    private static final int SUPERMICRO_X8 = 3;
    private static final int CORVALENT = 4;
    private static final int GLORIA = 5;
    private static final int MAXIME = 6;
    private static final int SUPERMICRO_X9_NODE = 7;
    private static final int SUPERMICRO_X9_STORAGE = 8;
    private static final int SUPERMICRO_X9_1U = 9;
    private static final int NUC = 10;
    private static final int REMOTE_LENOVO = 11;
    private static final int STEALTH = 12;
    private static final int SUPERMICRO_X10 = 13;
    private static final int WYSE = 14;
    private int code;

    private ComputerType(int code){
        this.code = code;
    }
    public int getCode() {
        return this.code;
    }
}