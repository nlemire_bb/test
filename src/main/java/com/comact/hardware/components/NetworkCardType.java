package com.comact.hardware.components;

/**
 * Created by dparadis on 2016-07-07.
 */
public enum NetworkCardType {

    Ipmi,
    Operation,
    Camera;
}
