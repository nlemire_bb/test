package com.comact.hardware.components.webrelay;

import com.comact.hardware.components.NetworkInterfaceCard;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import java.io.IOException;


/**
 * Created by dparadis on 2016-04-18.
 */
public class WebRelay{
    private static final Logger LOGGER = Logger.getLogger(WebRelay.class);
    private NetworkInterfaceCard nic;
    private WebRequestImpl webRelayStatusReq;
    private WebRelayState wrStateData;
    private final static String TURN_RELAY_ON_CMD = "?relayState=0 HTTP/1.1\\nAuthorization: Basic bm9uZTp3ZWJyZWxheQ==\\r\\n\\r\\n";
    private final static String TURN_RELAY_OFF_CMD = "?relayState=1 HTTP/1.1\\nAuthorization: Basic bm9uZTp3ZWJyZWxheQ==\\r\\n\\r\\n";
    private String RELAY_STATE_URL;
    private int cycleDelay = 5000;

    public WebRelay(NetworkInterfaceCard nic) {
        this.nic = nic;
        webRelayStatusReq = new WebRequestImpl();
        this.RELAY_STATE_URL = "http://" + nic.getIp() + "/state.xml";
    }

    public void setWebRelayStatusReqTest(WebRequestImpl webRelayStatusReq) {
        this.webRelayStatusReq = webRelayStatusReq;
    }

    public WebRelayStatus getStatus() throws Exception {
        if (nic.isReacheable()) {
            if (getState() != null)
                return WebRelayStatus.OK;
            else
                return WebRelayStatus.NOT_RESPONDING;
        }
        return WebRelayStatus.UNREACHEABLE;
    }

    public void turnON() throws Exception {
        try {

            webRelayStatusReq.sendCmd(RELAY_STATE_URL + TURN_RELAY_ON_CMD);

        } catch (IOException e) {
            throw new Exception("Could not send command to WebRelay",e);
        }

        if (getState().equals("OFF")){
            throw new Exception("State did not change");
        }

    }

    public void turnOFF() throws Exception {
        try {
            webRelayStatusReq.sendCmd(RELAY_STATE_URL + TURN_RELAY_OFF_CMD);
        } catch (IOException e) {
            throw new Exception("Could not send command to WebRelay",e);
        }
        if (getState().equals("ON")) {
            throw new Exception("State did not change");
        }
    }

    public void restart() throws Exception {
        turnOFF();
        try {
            Thread.sleep(cycleDelay);
        } catch (InterruptedException ex) {
            throw new Exception("could not sleep");
        }
        turnON();
    }


    public String getState() throws Exception {
        ByteArrayOutputStream stateData;
        String state = null;
        stateData = webRelayStatusReq.getXml(RELAY_STATE_URL);
        if (stateData != null) {
            XStream stream = new XStream();
            stream.processAnnotations(WebRelayState.class);
            try {
                wrStateData = (WebRelayState) stream.fromXML(stateData.toInputStream());
            } catch (Exception e) {
                LOGGER.warn("Bad or Corrupted data from webrelay");
            }
            if (wrStateData != null && wrStateData.getRelaystate() == 0) {
                state = "ON";
            } else if (wrStateData != null && wrStateData.getRelaystate() == 1) {
                state = "OFF";
            }
        } else throw new Exception("Could not get data from webrelay");
        return state;
    }
}
