package com.comact.hardware.components.webrelay;

/**
 * Created by dparadis on 2016-09-06.
 */
public enum WebRelayStatus {
    NOT_RESPONDING,
    UNREACHEABLE,
    OK;
}
