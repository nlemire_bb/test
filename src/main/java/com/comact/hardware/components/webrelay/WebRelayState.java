package com.comact.hardware.components.webrelay;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by dparadis on 2016-06-17.
 */
@XStreamAlias("datavalues")
public class WebRelayState {

    private Integer relaystate;
    private Integer inputstate;
    private Integer rebootstate;
    private Integer totalreboots;

    public Integer getRelaystate() {
        return relaystate;
    }

    public void setRelaystate(Integer relaystate) {
        this.relaystate = relaystate;
    }

    public Integer getInputstate() {
        return inputstate;
    }

    public void setInputstate(Integer inputstate) {
        this.inputstate = inputstate;
    }

    public Integer getRebootstate() {
        return rebootstate;
    }

    public void setRebootstate(Integer rebootstate) {
        this.rebootstate = rebootstate;
    }

    public Integer getTotalreboots() {
        return totalreboots;
    }

    public void setTotalreboots(Integer totalreboots) {
        this.totalreboots = totalreboots;
    }
}



