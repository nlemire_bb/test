package com.comact.hardware.components.webrelay;


import com.thoughtworks.xstream.XStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;


public class WebRequestImpl{
private boolean saveResponseToDisk = false;


    public void sendCmd(String url) throws IOException {
        URL urlObj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
        int responseCode = con.getResponseCode();
    }

    public ByteArrayOutputStream getXml(String url) throws URISyntaxException, IOException {

        URI remoteUrl = new URI(url);
        Socket socket = new Socket();
        InetAddress address = InetAddress.getByName(remoteUrl.getHost());
        int port = 80;

        socket.connect(new InetSocketAddress(address, port));
        if (socket.isConnected()) {

            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            outputStream.write("GET /state.xml HTTP/1.1\r\n\r\n".getBytes());
            outputStream.flush();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            IOUtils.copy(inputStream, byteArrayOutputStream);

            inputStream.close();
            outputStream.close();
            socket.close();
            if (saveResponseToDisk)savetodisk("response", byteArrayOutputStream);
            return byteArrayOutputStream;
        } else return null;

    }

    public void savetodisk(String filename, ByteArrayOutputStream byteArrayOutputStream) {
        try {
            XStream xstream = new XStream();
            xstream.toXML(byteArrayOutputStream, new FileOutputStream(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setSaveResponseToDisk(boolean saveResponseToDisk) {
        this.saveResponseToDisk = saveResponseToDisk;
    }
}

