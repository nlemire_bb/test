package com.comact.hardware.components;

/**
 * Created by dparadis on 2016-09-23.
 */
public enum ComputerState {
    ON,
    OFF,
    UNKNOWN;
}
