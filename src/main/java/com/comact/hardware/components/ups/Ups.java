package com.comact.hardware.components.ups;

import com.comact.hardware.components.CmocComponent;

/**
 * Created by dparadis on 2016-06-01.
 */
public interface Ups extends CmocComponent {
    String getStatus();
}
