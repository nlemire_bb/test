package com.comact.hardware.components.ups;

import com.comact.hardware.components.NetworkCardType;
import com.comact.hardware.protocol.SNMP.OID.ApcOID;
import com.comact.hardware.protocol.SNMP.SnmpManagerImpl;
import com.comact.hardware.components.NetworkInterfaceCard;
import org.snmp4j.PDU;
import org.snmp4j.smi.VariableBinding;

/**
 * Created by dparadis on 2016-05-01.
 */
public class UpsAPC implements Ups {
    private NetworkInterfaceCard nic;
    private SnmpManagerImpl snmp;

    public UpsAPC(String ip) {
        nic = new NetworkInterfaceCard(ip, NetworkCardType.Operation);
        if (nic.isReacheable()) {
            snmp = new SnmpManagerImpl(nic, "public");
        }
    }

    @Override
    public String getStatus() {

        VariableBinding upsStatusOID;
        upsStatusOID = ApcOID.BATTERY_STATUS.getVariableBinding();
        PDU pduUpsRequest = new PDU();
        pduUpsRequest.add(upsStatusOID);
        PDU responsePdu = null;
        try {
            responsePdu = snmp.getBulk(pduUpsRequest, "getBulk");
        } catch (Exception e) {
            e.printStackTrace();
        }
        String status = responsePdu.getVariableBindings().firstElement().toValueString();
        return status;
    }

    @Override
    public void restart() throws Exception {
    }

    @Override
    public void turnON() throws Exception {

    }

    @Override
    public void turnOFF() throws Exception {

    }
}
