package com.comact.hardware.components.networkSwitch;

import com.comact.hardware.protocol.SNMP.OID.CiscoOID;
import com.comact.hardware.protocol.SNMP.SnmpManager;
import com.comact.hardware.protocol.SNMP.TrapListener;
import com.comact.hardware.components.CmocComponent;
import org.apache.log4j.Logger;
import org.snmp4j.PDU;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by dparadis on 2016-05-01.
 */
public class Switch implements CmocComponent {
    private static final Logger LOGGER = Logger.getLogger(Switch.class);

    private int nbIf;
    private SnmpManager connection;
    private ArrayList<SwitchInterface> ports = new ArrayList<>();
    private ArrayList<SwitchInterface> vlans = new ArrayList<>();

    public Switch(SnmpManager connection) {
        this.connection = connection;
    }

    public String init() {
        if (isSnmpAccessible()) {
            initPortStatus();
            getMacTable();
            return "OK";

        } else return "Switch is Unreacheable";
    }

    public boolean isSnmpAccessible() {
        boolean snmpOk = false;
        PDU responseNbIFPDU;
        try {
            responseNbIFPDU = connection.getSingle(CiscoOID.IF_NUMBER.getOid());
            if (responseNbIFPDU != null) {
                nbIf = responseNbIFPDU.getVariable(CiscoOID.IF_NUMBER.getOid()).toInt();

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Nbr Interface = " + nbIf);
                }

                snmpOk = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return snmpOk;
    }

    //***

    private void initPortStatus() {

        try {
            //setup PDU for IF status
            PDU pduGetIfStatus = new PDU();

            VariableBinding[] oidlist = new VariableBinding[]{
                    CiscoOID.IF_INDEX.getVariableBinding(),
                    CiscoOID.IF_DESCRIPTION.getVariableBinding(),
                    CiscoOID.IF_OPRER_STATUS.getVariableBinding(),
                    CiscoOID.IF_SPEED.getVariableBinding(),
                    CiscoOID.IF_TYPE.getVariableBinding()};

            pduGetIfStatus.addAll(oidlist);

            //get First Bulk Data
            PDU pduResponseIfStatus = connection.getBulk(pduGetIfStatus, "Next");

            if (pduResponseIfStatus != null) {

                int nextPortNum = 1;
                SwitchInterface currentIf;
                Vector<? extends VariableBinding> responseData;
                responseData = pduResponseIfStatus.getVariableBindings();

                while (responseData.elementAt(0).getOid().startsWith(CiscoOID.IF_INDEX.getOid())) {
                    currentIf = new SwitchInterface();
                    currentIf.setIndex(responseData.elementAt(0).getVariable().toInt());
                    currentIf.setOid(responseData.elementAt(0).getOid());
                    currentIf.setName(responseData.elementAt(1).toValueString());
                    currentIf.setOperationStatus(responseData.elementAt(2).getVariable().toInt());
                    currentIf.setSpeed(responseData.elementAt(3).getVariable().toLong());
                    currentIf.setType(responseData.elementAt(4).getVariable().toInt());

                    //If it is a physical port (type 6), set port number
                    if (currentIf.getType() == 6) {
                        currentIf.setVlan(getPortVlan(currentIf.getIndex()));
                        currentIf.setPortNum(nextPortNum);
                        ports.add(currentIf);
                        nextPortNum++;
                        //if it is a Vlan (type 53)then use the vlan Array
                    } else if (currentIf.getType() == 53) {
                        currentIf.setVlan(currentIf.getIndex());
                        vlans.add(currentIf);
                    }

                    //retrieve the current OID and insert in new get PDU
                    pduGetIfStatus.clear();
                    for (int currentVariable = 0; currentVariable <= (responseData.size()) - 1; currentVariable++) {
                        OID currentOID = responseData.elementAt(currentVariable).getOid();
                        pduGetIfStatus.add(new VariableBinding(new OID(currentOID)));
                    }
                    //get Next Bulk
                    pduResponseIfStatus = connection.getBulk(pduGetIfStatus, "Next");
                    responseData = pduResponseIfStatus.getVariableBindings();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getPortVlan(int ifIndex) {
        PDU pduGetVlan = new PDU();
        OID oid = new OID(CiscoOID.IF_VLAN.getOid() + "." + ifIndex);
        VariableBinding vb = new VariableBinding(oid);
        pduGetVlan.add(vb);
        PDU pduResponsePortVlan;
        try {
            pduResponsePortVlan = connection.getBulk(pduGetVlan, "get");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return pduResponsePortVlan.getVariableBindings().firstElement().getVariable().toInt();
    }


    private void getMacTable() {
        //get MAC Address Table
        PDU pduResponseMacTbl;
        OID currentOID;
        PDU pduGetMacTbl = new PDU();
        pduGetMacTbl.add(CiscoOID.MACADDR.getVariableBinding());
        pduGetMacTbl.add(CiscoOID.MACPORT.getVariableBinding());

        try {
            pduResponseMacTbl = connection.getBulk(pduGetMacTbl, "Next");
            String mac;
            int port;
            //put response array in variable
            Vector<? extends VariableBinding> responseData;

            responseData = pduResponseMacTbl.getVariableBindings();
            while (responseData.elementAt(0).getOid().startsWith(CiscoOID.MACADDR.getOid())) {
                // get port associated with the mac
                port = responseData.elementAt(1).getVariable().toInt();
                //Set MAC to the right Port
                mac = responseData.elementAt(0).toValueString();
                //
                SwitchInterface targetport = getSinglePort(port);
                targetport.addConnectedDevice(mac);

                pduGetMacTbl.clear();
                for (int currentVariable = 0; currentVariable <= (responseData.size()) - 1; currentVariable++) {
                    currentOID = responseData.elementAt(currentVariable).getOid();
                    pduGetMacTbl.add(new VariableBinding(new OID(currentOID)));
                }
                //get Next Bulk
                pduResponseMacTbl = connection.getBulk(pduGetMacTbl, "Next");
                responseData = pduResponseMacTbl.getVariableBindings();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SwitchInterface getSinglePort(int port) {
        SwitchInterface obj = null;
        for (int currentInterface = 0; currentInterface < ports.size(); currentInterface++) {
            obj = ports.get(currentInterface);
            if (obj.getPortNum() == port)
                return obj;
        }
        return obj;
    }

    public ArrayList<SwitchInterface> getPorts() {
        return ports;
    }

    public boolean getSinglePortStatus(int port) {
        return getSinglePort(port).isStatus();
    }

    public void backupConfig() {
      /*  try {
            TelnetClientConnection tn = new TelnetClientConnection();
            tn.connect(switchip, 23);
            tn.registerNotifHandler();
            tn.startReader();
            tn.login("mm98","comact");
            //tn.restoreConfig("BackupConfig19mai16_14_59");
            tn.backupConfig();
            tn.logout();
            //tn.emulateTerm();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (listenToSnmpTraps) {
            myswitch.listentoSwitch(localTrapAddress +"/162");
        }*/
    }

    public void listentoSwitch(String ip) {
        TrapListener snmp4jTrapReceiver = new TrapListener();
        try {
            snmp4jTrapReceiver.listen(new UdpAddress(ip));

        } catch (IOException e) {
            System.err.println("Error in Listening for Trap");
            System.err.println("Exception Message = " + e.getMessage());
        }
    }

    @Override
    public String getStatus() {
        return null;
    }

    @Override
    public void restart() throws Exception {

    }

    @Override
    public void turnON() throws Exception {

    }

    @Override
    public void turnOFF() throws Exception {

    }
}



