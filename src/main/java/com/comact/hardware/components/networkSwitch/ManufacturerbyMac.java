package com.comact.hardware.components.networkSwitch;

/**
 * Created by dparadis on 2016-05-11.
 */
public enum ManufacturerbyMac {

    SUPERMICRO("00:25", "SuperMicro", "Computer"),
    WEBRELAY("00:0c:c8", "WebRelay", "Remote controlled Relay"),
    BASLER("00:30:53", "Basler", "Camera Basler"),
    INTEL("10:0b:a9", "Intel", "Miscellaneous"),
    APEX("00:02:99", "Apex", "Liebert UPS network Card"),
    GENIE("00:01:0d", "Coreco", "Camera Genie");

    private final String macHeader;
    private final String manufacturer;
    private final String type;

    ManufacturerbyMac(String macHeader, String manufacturer, String type) {
        this.macHeader = macHeader;
        this.manufacturer = manufacturer;
        this.type = type;
    }

    public String getMacHeader() {
        return macHeader;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getType() {
        return type;
    }
}

