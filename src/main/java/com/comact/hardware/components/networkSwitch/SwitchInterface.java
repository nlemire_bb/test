package com.comact.hardware.components.networkSwitch;

import org.snmp4j.smi.OID;

import java.util.ArrayList;

/**
 * Created by dparadis on 2016-05-03.
 */
public class SwitchInterface {
    private String name;
    private int index;
    private int type;
    private int operationStatus;
    private long speed;
    private OID oid;
    private int portNum;
    private boolean status;
    private int vlan;
    private ArrayList<ConnectedDevice> connectedDevices = new ArrayList<>();

    public SwitchInterface() {
        status = true;
    }

    //<editor-fold desc="Setters and Getters">
    //setter and getters

    public int getPortNum() {
        return portNum;
    }

    public void setPortNum(int portNum) {
        this.portNum = portNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(int operationStatus) {
        this.operationStatus = operationStatus;
    }

    public long getSpeed() {
        return speed;
    }

    public void setSpeed(long speed) {
        this.speed = speed;
    }

    public OID getOid() {
        return oid;
    }

    public void setOid(OID oid) {
        this.oid = oid;
    }

    public int getVlan() {
        return vlan;
    }

    public void setVlan(int vlan) {
        this.vlan = vlan;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ArrayList<ConnectedDevice> getConnectedDevices() {
        return connectedDevices;
    }

    //</editor-fold>

    public void addConnectedDevice(String mac) {

        connectedDevices.add(new ConnectedDevice(mac));
    }
}
