package com.comact.hardware.components.networkSwitch;

/**
 * Created by dparadis on 2016-05-11.
 */
public class ConnectedDevice {

    private static final ManufacturerbyMac[] MANUFACTURERS = ManufacturerbyMac.values();
    private String macAddr;
    private String manufacturer;
    private String deviceType;

    public ConnectedDevice(String macAddr) {
        this.macAddr = macAddr;
        for (int currentItem = 0; currentItem < MANUFACTURERS.length; currentItem++) {
            if (macAddr.startsWith(MANUFACTURERS[currentItem].getMacHeader())) {
                manufacturer = (MANUFACTURERS[currentItem].getManufacturer());
                deviceType = (MANUFACTURERS[currentItem].getType());
                break;
            }
        }
    }

    public String getMacAddr() {
        return macAddr;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getDeviceType() {
        return deviceType;
    }

}
