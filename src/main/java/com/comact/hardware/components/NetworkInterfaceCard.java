package com.comact.hardware.components;

import java.io.IOException;
import java.net.*;
import java.util.Date;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by dparadis on 2016-06-30.
 */
public class NetworkInterfaceCard {

    private NetworkCardType type;
    protected String ip;
    protected int port;
    private String macAddr;
    private boolean isReacheable;
    private Date lastStatusUpdate = null;
    private long MAX_DURATION = MILLISECONDS.convert(1, SECONDS);

    public NetworkInterfaceCard(String ip, NetworkCardType type) {
        this.ip = ip;
        this.type = type;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public NetworkCardType getType() {
        return type;
    }

    public void setType(NetworkCardType type) {
        this.type = type;
    }

    public boolean isReacheable() {
        //test socket only if the last test was more than 5 seconds ago to prevent network spamming.
        if (lastStatusUpdate == null || new Date().getTime() - lastStatusUpdate.getTime() > MAX_DURATION) {
            testSocket();
            if (!isReacheable) {
                testInet();
            }
        }
        return isReacheable;
    }

    public void setReacheable(boolean reacheable) {
        this.isReacheable = reacheable;
    }

    public String getMacAddr() {
        return macAddr;
    }

    public void setMacAddr(String macAddr) {
        this.macAddr = macAddr;
    }

    public Date getLastUpdate() {
        return lastStatusUpdate;
    }

    public void setLastUpdate() {
        this.lastStatusUpdate = new Date();
    }

    private void testSocket() {
        this.lastStatusUpdate = new Date();
        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(ip, 80), 3000);
                isReacheable = true;
            }
        } catch (IOException ex) {

            isReacheable = false;
        }
    }

    private void testInet() {

        try {
            InetAddress addr = InetAddress.getByName(ip);
            if (addr.isReachable(2000)) {
                isReacheable = true;
            }
        } catch (IOException e) {
            isReacheable = false;
        }
    }
}
