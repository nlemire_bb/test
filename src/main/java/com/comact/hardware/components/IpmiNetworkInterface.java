package com.comact.hardware.components;
/*
 *
 */

import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.PrivilegeLevel;
import com.veraxsystems.vxipmi.coding.commands.chassis.ChassisControl;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatus;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatusResponseData;
import com.veraxsystems.vxipmi.coding.commands.chassis.PowerCommand;
import com.veraxsystems.vxipmi.coding.commands.session.GetChannelAuthenticationCapabilitiesResponseData;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;
import org.apache.log4j.Logger;

import java.net.InetAddress;

public class IpmiNetworkInterface extends NetworkInterfaceCard {
    private static final Logger LOGGER = Logger.getLogger(IpmiNetworkInterface.class);
    private int port;
    private IpmiConnector connector;
    private ConnectionHandle handle;
    private CipherSuite cs;
    private GetChassisStatus chassisStatus;
    private GetChassisStatusResponseData rd;
    private String userName;
    private String password;
    private boolean sessionActive;

    //<editor-fold desc="Setters n Getters">
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected void setConnector(IpmiConnector connector) {
        this.connector = connector;
    }

    protected void setCs(CipherSuite cs) {
        this.cs = cs;
    }

    protected void setHandle(ConnectionHandle handle) {
        this.handle = handle;
    }

    protected void setChassisStatus(GetChassisStatus chassisStatus) {
        this.chassisStatus = chassisStatus;
    }

    //</editor-fold>

    public IpmiNetworkInterface(String ip, NetworkCardType type, int port, String username, String password) {
        super(ip, type);
        this.port = port;
        this.userName = username;
        this.password = password;
        this.sessionActive = false;
    }

    private void verifyConnection() throws Exception {
        try {
            if (isReacheable()) {
                if (connector == null) {
                    initIpmiConnector();
                }
                if (!sessionActive) {
                    openIpmiSession();
                }
            } else throw new Exception("Can't open Socket to " + ip);

        } catch (Exception e) {
            if (sessionActive)
                disconnect();
            throw new Exception("Could Not Connect to IPMI on:" + ip+" "+e.toString());
        }
    }

    private void openIpmiSession() throws Exception {
        try {
            GetChannelAuthenticationCapabilitiesResponseData data = connector.getChannelAuthenticationCapabilities(handle, cs, PrivilegeLevel.Administrator);
            connector.openSession(handle, userName, password, null);
            sessionActive = true;
        } catch (Exception e) {
            LOGGER.error("Could not open IPMI Session");
            throw new Exception("Could not open IPMI Session", e);
        }
    }

    private void initIpmiConnector() throws Exception {
        try {
            // Create the connector, specify outboundPort that will be used to communicate
            // with the remote host. The UDP layer starts listening at this outboundPort, so
            // no 2 connectors can work at the same time on the same outboundPort.
            if (connector == null)
                connector = new IpmiConnector(port);

            // Create the connection and get the handle, specify IP address of the
            // remote host. The connection is being registered in ConnectionManager,
            // the handle will be needed to identify it among other connections
            // (target IP address isn't enough, since we can handle multiple
            // connections to the same host)
            if (handle == null)
                handle = connector.createConnection(InetAddress.getByName(ip));

            connector.setTimeout(handle, 5000);

            // Get available cipher suites list via getAvailableCipherSuites and
            // pick one of them that will be used further in the session.

            if (cs == null)
                cs = connector.getAvailableCipherSuites(handle).get(1);

            if (chassisStatus == null)
                chassisStatus = new GetChassisStatus(IpmiVersion.V20, cs, AuthenticationType.RMCPPlus);


        } catch (Exception e) {
            sessionActive = false;
            LOGGER.error("Error while initializing connector: " + e.toString());
            throw new Exception("Could not initialize connector", e);
        }
    }

    private void closeIpmiSession() throws Exception {
        connector.closeSession(handle);
        sessionActive = false;
    }

    private void closeIpmiConnection() throws Exception {
        connector.closeConnection(handle);
        sessionActive = false;
    }

    private void disconnect() throws Exception {
        // Close the session
        try {
            closeIpmiSession();
            closeIpmiConnection();
            connector.tearDown();
            connector = null;
            handle = null;
            cs = null;


        } catch (Exception e) {
            sessionActive = false;
            throw new Exception("Could not disconnect from IPMI", e);
        }
    }

    private void getChassisData() throws Exception {
        try {
            rd = (GetChassisStatusResponseData) connector.sendMessage(handle, chassisStatus);
        } catch (Exception e) {
            if (sessionActive)
                disconnect();
            throw new Exception("Could not get Data From Chassis", e);
        }

    }

    public ComputerState getState() {
        try {
            verifyConnection();
            getChassisData();
            if (rd.isPowerOn())
                return ComputerState.ON;
            else if (!rd.isPowerOn())
                return ComputerState.OFF;
            else
                return ComputerState.UNKNOWN;

        } catch (Exception e) {
            sessionActive = false;
            return ComputerState.UNKNOWN;
        }
    }

    public void turnON(int delay) throws Exception {
        verifyConnection();
        getChassisData();
        boolean currentstate = rd.isPowerOn();
        if (!currentstate) {
            sendIPMIPowerCommand(currentstate, PowerCommand.PowerUp, delay);
        } else {
            throw new Exception("Computer Already ON");
        }
    }

    public void turnOFF(int delay) throws Exception {
        verifyConnection();
        getChassisData();
        boolean currentState = rd.isPowerOn();
        if (currentState) {
            sendIPMIPowerCommand(currentState, PowerCommand.PowerDown, delay);
        } else {
            throw new Exception("Computer Already OFF");
        }
    }

    private void sendIPMIPowerCommand(boolean currentstate, PowerCommand powerCMD, int delay) throws Exception {

        ChassisControl chassisControl = new ChassisControl(IpmiVersion.V20, cs, AuthenticationType.RMCPPlus, powerCMD);
        // sending command
        try {
            connector.sendMessage(handle, chassisControl);
        } catch (Exception e) {
            LOGGER.warn("Could not send message to Target");
            throw new Exception("Could not send message to Target", e);
        }
        //checking if command was applied
        for (int i = 0; i < 10; i++) {
            getChassisData();
            if (rd.isPowerOn() == currentstate) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    LOGGER.warn("Thread could not sleep");
                    throw new Exception("Thread could not sleep");
                }
                i++;
            } else return;
        }
        throw new Exception("Computer never changed state");
    }


}


